#' Creates effective length feature
#' @import data.table
#' @importFrom GenomicRanges findOverlaps GRanges seqnames start end
#' @param chr A string specifying the chromosome
#' @param regions GRanges object of genomic regions that partition the chromosome
#' @param genome The reference genome
#' @param version The version of reference genome
#' @param pattern The restriction enzyme signature
#' @return A numeric vector of mean effective length scores
#' @export
#'
Len <- function(chr, regions, genome="Hsapiens", version="hg19", pattern="AAGCTT"){

  package <- sprintf('BSgenome.%s.UCSC.%s', genome, version)
  library(package, character.only = TRUE)

  cuts <- GRanges(seqnames=chr, ranges=ranges(matchPattern(pattern, get(genome, mode="any")[[chr]])))
  medians <- apply(cbind(start(cuts), end(cuts)), 1, median)
  FragmentendsL <- GRanges(seqnames = seqnames(cuts), ranges = IRanges(end = medians - 1, width = 500))
  FragmentendsR <- GRanges(seqnames = seqnames(cuts), ranges = IRanges(start = medians + 1, width = 500))
  hitsL <- findOverlaps(FragmentendsL, regions, type = "within", select = "all")
  hitsR <- findOverlaps(FragmentendsR, regions, type = "within", select = "all")
  L <- data.table(bins=names(regions[subjectHits(hitsL)]), widths=width(FragmentendsL[queryHits(hitsL)]))
  l <- L[, list(width=sum(widths)),bins]
  R <- data.table(bins=names(regions[subjectHits(hitsR)]), widths=width(FragmentendsL[queryHits(hitsR)]))
  r <- R[,list(width=sum(widths)),bins]
  setkey(l,"bins")
  result <- merge(l,r,all = TRUE)
  result <- result[, list(bins=bins,width=apply(cbind(width.x, width.y), 1,
                                                function(x){sum(x,na.rm = TRUE)}))]
  result <- rbind(result,data.table(bins=setdiff(names(regions),result$bins),width=0))
  result <- as.data.frame(result)
  row.names(result) <- result$bins
  result <- result[names(regions),]
  result <- as.data.table(result)
  return(result$width)
  rm(list=ls())
  gc()
}
